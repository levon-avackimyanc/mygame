// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MyCourseProject/MyFloorSpawn.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyFloorSpawn() {}
// Cross Module References
	MYCOURSEPROJECT_API UClass* Z_Construct_UClass_AMyFloorSpawn_NoRegister();
	MYCOURSEPROJECT_API UClass* Z_Construct_UClass_AMyFloorSpawn();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	UPackage* Z_Construct_UPackage__Script_MyCourseProject();
// End Cross Module References
	void AMyFloorSpawn::StaticRegisterNativesAMyFloorSpawn()
	{
	}
	UClass* Z_Construct_UClass_AMyFloorSpawn_NoRegister()
	{
		return AMyFloorSpawn::StaticClass();
	}
	struct Z_Construct_UClass_AMyFloorSpawn_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyFloorSpawn_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawn,
		(UObject* (*)())Z_Construct_UPackage__Script_MyCourseProject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyFloorSpawn_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "MyFloorSpawn.h" },
		{ "ModuleRelativePath", "MyFloorSpawn.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyFloorSpawn_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyFloorSpawn>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyFloorSpawn_Statics::ClassParams = {
		&AMyFloorSpawn::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyFloorSpawn_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyFloorSpawn_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyFloorSpawn()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyFloorSpawn_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyFloorSpawn, 2999132144);
	template<> MYCOURSEPROJECT_API UClass* StaticClass<AMyFloorSpawn>()
	{
		return AMyFloorSpawn::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyFloorSpawn(Z_Construct_UClass_AMyFloorSpawn, &AMyFloorSpawn::StaticClass, TEXT("/Script/MyCourseProject"), TEXT("AMyFloorSpawn"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyFloorSpawn);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
