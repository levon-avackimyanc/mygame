// Fill out your copyright notice in the Description page of Project Settings.


#include "MyWall.h"
#include "Components/BoxComponent.h"
#include "UObject/ConstructorHelpers.h"

// Sets default values
AMyWall::AMyWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	FloorMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Cube")).Object;
	FloorColor = ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("Material'/Game/StarterContent/Materials/M_Concrete_Poured.M_Concrete_Poured'")).Object;
	RootComponent = MyRootComp;
	class UStaticMeshComponent* FloorComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Floor"));
	FloorComp->SetStaticMesh(FloorMesh);
	FloorComp->SetRelativeRotation(FRotator(90, 0, 90));
	FloorComp->SetRelativeLocation(FVector(0, 0, 0));
	FloorComp->SetRelativeScale3D(FVector(9.0f, 22.0f, 1.0f));
	FloorComp->SetMaterial(0, FloorColor);
	FloorComp->AttachTo(MyRootComp);
}

// Called when the game starts or when spawned
void AMyWall::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void AMyWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Buff += DeltaTime;
	if (Buff > Delay) {
		Destroy(true, true);
		Buff = 0;
	}
}

