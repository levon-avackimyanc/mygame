// Fill out your copyright notice in the Description page of Project Settings.


#include "MyFloorSpawn.h"
#include "MyFloorActor.h"
#include "MyWall.h"
// Sets default values
AMyFloorSpawn::AMyFloorSpawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void AMyFloorSpawn::AddFloor()
{
	FRotator NewRot = FRotator(0, 0, 0);
	FVector NewVec(X + Width, 0, SpawnZ);
	FVector WallVec1(X + Width, WallY, WallZ);
	FVector WallVec2(X + Width, -WallY, WallZ);
	if (GetWorld()) {
		GetWorld()->SpawnActor<AMyFloorActor>(NewVec, NewRot);
		GetWorld()->SpawnActor<AMyWall>(WallVec1, NewRot);
		GetWorld()->SpawnActor<AMyWall>(WallVec2, NewRot);
		X += Width;
	}
}

// Called when the game starts or when spawned
void AMyFloorSpawn::BeginPlay()
{
	Super::BeginPlay();
	FVector FirstSpawn = FVector(X, 0, SpawnZ);
	FVector WallSpawn1 = FVector(X, WallY, WallZ);
	FVector WallSpawn2 = FVector(X, -WallY, WallZ);
	FRotator FirstRot = FRotator(0, 0, 0);
	if (GetWorld()) {
		GetWorld()->SpawnActor<AMyFloorActor>(FirstSpawn, FirstRot);
		GetWorld()->SpawnActor<AMyWall>(WallSpawn1, FirstRot);
		GetWorld()->SpawnActor<AMyWall>(WallSpawn2, FirstRot);
	}
}

// Called every frame
void AMyFloorSpawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	BuffTimer += DeltaTime;
	if (BuffTimer > Delay) {
		AddFloor();
		BuffTimer = 0;
	}
}

// Called to bind functionality to input
void AMyFloorSpawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

